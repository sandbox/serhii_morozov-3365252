<?php

namespace Drupal\domain_keys\Plugin\KeyProvider;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\key\KeyInterface;
use Drupal\key\Plugin\KeyProviderBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyPluginFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a key provider that stores key for each domain.
 *
 * @KeyProvider(
 *   id = "domain_keys",
 *   label = @Translation("Domain keys"),
 *   description = @Translation(""),
 *   storage_method = "domain_keys",
 *   key_value = {
 *     "accepted" = FALSE,
 *     "required" = FALSE
 *   }
 * )
 */
class DomainKeys extends KeyProviderBase implements KeyPluginFormInterface, ContainerFactoryPluginInterface {
  /**
   * The domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiator
   */
  protected $domainNegotiator;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    $domainNegotiator,
    $configFactory,
    $routeMatch
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->domainNegotiator = $domainNegotiator;
    $this->configFactory = $configFactory;
    $this->routeMatch = $routeMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $domainNegotiator = $container->get('domain.negotiator');
    $configFactory = $container->get('config.factory');
    $routeMatch = $container->get('current_route_match');
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $domainNegotiator,
      $configFactory,
      $routeMatch
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if ($key_type = $this->routeMatch->getParameter('key')) {
      $key_id = $key_type->id();
      $url = Url::fromRoute('domain_keys.keys_edit', ['key_type' => $key_id])
        ->toString();
    }
    else {
      $url = Url::fromRoute('domain_keys.collection')->toString();
    }

    $form['link'] = [
      '#type' => 'tag',
      '#markup' => $this->t('The domain keys provider allows settings a key value for each domain, refer to the <a href="@url">domain keys page</a>', ['@url' => $url]),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyValue(KeyInterface $key) {
    $domain = $this->domainNegotiator->getActiveId();
    // Load the key value for the current domain.
    $config = $this->configFactory->get('domain_keys.keys.' . $key->id());
    return $config->get('values')[$domain] ?? '';
  }

}
