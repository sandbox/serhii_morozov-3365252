<?php

namespace Drupal\domain_keys\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines dynamic local tasks.
 */
class DomainKeysLocalTask extends DeriverBase implements ContainerDeriverInterface {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The key.services.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   *   The domain keys service
   */
  protected $keyRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct($keyRepository, $entityTypeManager) {
    $this->keyRepository = $keyRepository;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('key.repository'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $key_ids = $this->keyRepository->getKeysByProvider('domain_keys');
    foreach ($key_ids as $key => $value) {
      $this->derivatives[$key] = $base_plugin_definition;
      $this->derivatives[$key]['title'] = $value->label();
      $this->derivatives[$key]['route_parameters']['key_type'] = $key;
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
