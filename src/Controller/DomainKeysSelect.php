<?php

namespace Drupal\domain_keys\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\key\KeyRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides selection of keys to change.
 */
class DomainKeysSelect extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The key.services.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   *   The domain keys service
   */
  protected $keyRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct(KeyRepositoryInterface $keyRepository) {
    $this->keyRepository = $keyRepository;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('key.repository')
    );
  }

  /**
   * Returns a page with existing keys.
   *
   * @return array
   *   Renderable array.
   */
  public function keysEnabled() {
    $items = [];
    foreach ($this->keyRepository->getKeysByProvider('domain_keys') as $key) {
      $items[] = Link::createFromRoute(
        $key->label(),
        'domain_keys.keys_edit',
        ['key_type' => $key->id()]
      );
    }

    $build['list'] = [
      '#title' => $this->t('Choose a key to edit'),
      '#theme' => "item_list",
      '#items' => $items,
      '#empty' => $this->t(
        'No Domain Keys defined, visit the <a href="@url">Key admin page</a> to create one.',
        ['@url' => Url::fromRoute('entity.key.collection')->toString()]
      ),
    ];

    return $build;
  }

}
