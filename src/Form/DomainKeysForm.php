<?php

namespace Drupal\domain_keys\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\key\KeyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Configure Dagens perspektiv - Domain functionality settings for this site.
 */
class DomainKeysForm extends ConfigFormBase implements ContainerInjectionInterface {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The domain_keys.services.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->keyRepository = $container->get('key.repository');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_keys_edit';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $config_names = [];

    /** @var \Drupal\key\KeyInterface|null $key */
    $key = $this->getRouteMatch()->getParameter('key_type');
    if ($key) {
      $config_names[] = 'domain_keys.keys.' . $key->id();
    }

    return $config_names;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, KeyInterface $key_type = NULL) {
    if (!$key_type) {
      throw new AccessDeniedException("Key parameter is required.");
    }

    $form['#title'] = $this->t('Edit domain key "@key"', ['@key' => $key_type->label()]);

    $form['key_type'] = [
      '#type' => 'value',
      '#value' => $key_type->id(),
    ];

    $form['key_values'] = [
      '#title' => $this->t("Key values"),
      '#type' => 'fieldset',
      '#tree' => TRUE,
    ];

    /** @var \Drupal\domain\DomainStorageInterface $domain_storage */
    $domain_storage = $this->entityTypeManager->getStorage('domain');
    $key_values = $this->config('domain_keys.keys.' . $key_type->id())->get('values');
    foreach ($domain_storage->loadMultipleSorted() as $domain) {
      $domain_id = $domain->id();
      $form['key_values'][$domain->id()] = [
        '#type' => 'textfield',
        '#title' => $domain->label(),
        '#required' => TRUE,
        '#default_value' => $key_values[$domain_id] ?? NULL,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState, KeyInterface $key_type = NULL) {
    parent::submitForm($form, $formState);

    $config_name = 'domain_keys.keys.' . $formState->getValue('key_type');
    $this->config($config_name)
      ->set('values', $formState->getValue('key_values'))
      ->save();

    $this->messenger()
      ->addStatus('Key values saved for all domains');
  }

}
